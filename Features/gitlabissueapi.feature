Feature: GitLab Issues API test scenarios for ABN AMRO Assignment
  I wrote 6 test scenarios to cover CRUD operations (POST, GET, PUT and DELETE) plus edge cases for GITLAB Issues API
  
  #Initializing personal access token and Project id through Datatable
  Background: Initialize some of common properties / values such as Access Token and Project id
  	Given Initializing Access Token and Project id
  	|AccessToken|glpat-hxrjxtx8JhsqTy7fpCRx|
  	|projectID	|36217168|
  
  Scenario: Get all issues and Count the number of issues in GitLab project, create new Issue then get and validate count of issues incremented by 1
    Given Get all the issues 
    And Count the issues 
    When Create Issue in GitLab 
		|title 				|B      | 
		|description	|This bug1 is getting created through API|
		|statuscode 	|201 |
		Then Get all the issues 
		And Verify count of Issues should increment by one 
		
	Scenario: Get all issues and Find an issue which is in open, update title and state of issue as close and after update, verify state of issue is close
		Given Get all the issues 
		And Find an existing issue with state 
		|state|opened|
   	When Update title and state event of issue 
   	|title|djksfhsdjkfkkkkkkkkkkkjdddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd|
  	|state|close| 
  	|statuscode 	|200 |
  	Then Verify title state of issue 
  	|state|closed|
  	
  Scenario: Get an existing issue which is in close and Delete the Issue, then again get the issue and verify the issue is still exist in GitLab
   	Given Get all the issues
   	And Find an existing issue with state
   	|state|closed|
   	When Delete the issue
   	|statuscode|204|
   	Then Get the issue 
   	And verify the issue deleted 
   	
  Scenario: Subscribe to an issue which is already subscribed and validate 304 status code in reponse
   	Given Get all the issues
   	And Find an existing issue with state
   	|state|opened|
   	When Subscribe to an issue
   	Then Verify the status code
   	|statuscode|304|
 
   Scenario: Move an issue to same project where issue is currently in then verify response code as 400 and response time of api is below 300 milliseconds
   	Given Get all the issues
   	And Find an existing issue with state
   	|state|opened|
   	When Move an issue
   	Then Verify the status code and response message
   	|statuscode|400|
   	|message|Cannot move issue to project it originates from!|
  	And verify response time of move issue api
   	|resptime|1000|
   	
  Scenario: Get all issues and Count the number of issues in GitLab project, create issue with title as non-string
    Given Get all the issues 
 	  And Count the issues 
    When Create Issue in GitLab with title non_string
		|title 				|GJ($ | 
		|description	|This bug1 is getting created through API|
		Then Verify the status code
		|statuscode 	|400 |
