package testRunner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = ".//Features/gitlabissueapi.feature",
		glue = "stepDefinitions",
		monochrome = true, // step definitions should be more readable and removed unreadable characters
		plugin = {"pretty","html:test-output.html"} // create html ouput
		)

public class TestRunner {

}
