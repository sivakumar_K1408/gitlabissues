package stepDefinitions;

import static io.restassured.RestAssured.*;
import java.util.List;

import org.hamcrest.Matchers;
import org.testng.Assert;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.*;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.ValidatableResponse;



public class IssueApi {
	
	String baseURL = "https://gitlab.com/api/v4/";
	String bearerToken;
	String title;
	String description;
	int statuscode;
	ValidatableResponse response;
	int current_number_of_issues;
	String projectID;
	String issue_id;
	
	@Given("Initializing Access Token and Project id")
	public void initializing_access_token_and_Project_id(DataTable testdata) throws Throwable {
		//Get the Data table from cucumber feature file into List
		List<List<String>> data = testdata.asLists(String.class);
		
		//get the data table's data into individual variables like personal access token and project id
		bearerToken = data.get(0).get(1);
		projectID = data.get(1).get(1);
				
		  RestAssured.baseURI = baseURL; // Assign baseURL
		  
		 
	}
	
	@When("Create Issue in GitLab")
	public void create_issue_in_gitlab(DataTable testdata) throws Throwable {
		//Get the Data table from cucumber feature file into List
		List<List<String>> data = testdata.asLists(String.class);
		
		//get the data table's data into individual variables like title of the issue
		title = data.get(0).get(1);
		description = data.get(1).get(1); //description of the issue
		statuscode = Integer.parseInt(data.get(2).get(1)); //status code
  
		//create new issue using post issues api
		  response = given()
		  .queryParam("title", "Issues with auth") 
		  .queryParam("labels", "bug")
		  .header("Authorization", "Bearer " + bearerToken)
		  .header("Content-Type","application/json") 
		  .body("{\r\n"
					+ "\"title\":\""+title+"\",\r\n"
					+ "\"description\":\""+description+"\"\r\n"
					+ "}\r\n"
					+ "")
		  .when().post("projects/"+projectID+"/issues")
		  .then().log().all()
		  .statusCode(statuscode);	  //verify the response status code with data table value
		 
	}
	
	@Given("Get all the issues")
	public void get_all_the_issues() throws Throwable {
		  
		//Get all the issues using get issues api
		response = given()
		  .header("Authorization", "Bearer " + bearerToken)
		  .when().get("issues/")
		  .then();
		 
	}
	
	@And("Count the issues")
	public void Count_the_issues() throws Throwable{
		//the expected size of json array in get response with all issues from project
		 current_number_of_issues = response.extract().jsonPath().getList("$").size();
		  System.out.println("number of the Issues: " + current_number_of_issues);
		 
	}
	
	@And("Verify count of Issues should increment by one")
	public void verify_count_of_issues_should_increment_by_one() throws Throwable {
		// actual size of json array after new issue gets created
		 int new_current_number_of_issues = response.extract().jsonPath().getList("$").size();
		 Assert.assertEquals(new_current_number_of_issues, current_number_of_issues+1);
		 
	}
	
	@And("Find an existing issue with state")
	public void find_an_existing_issue_with_state(DataTable testdata) throws Throwable {
		//Get the Data table from cucumber feature file into List
		List<List<String>> data = testdata.asLists(String.class);
		//get the issue with state opened value from datatable
		String expected_state_of_issue = data.get(0).get(1);
		
		//size of the issues json array
		int number_of_issues = response.extract().jsonPath().getList("$").size();
		JsonPath resp = response.extract().body().jsonPath(); // jsonpath helps to extract nodes of json response
		for (int i = 0; i < number_of_issues;i++ )
		{   // condition to check state of issue in response matches with opened state
			if (expected_state_of_issue.equals(resp.getJsonObject("["+i+"].state").toString()))
			{ //if state is open then get issue id of it
				issue_id = resp.getJsonObject("["+i+"].iid").toString();
				System.out.println("issue_id: "+ issue_id);
				break; // exit the for loop
			}
		}
		
	}
	
	@Given("Update title and state event of issue")
	public void update_title_and_state_event_of_issue(DataTable testdata) throws Throwable {
		String newstate;
		int iss_id = Integer.parseInt(issue_id);
		//Get the Data table from cucumber feature file into List
		List<List<String>> data = testdata.asLists(String.class);
		
		title = data.get(0).get(1); // get title to update issue
		newstate = data.get(1).get(1); // get new state to update the issue
		statuscode = Integer.parseInt(data.get(2).get(1)); // response status code
		// update the issue using put api, updating title name and state of issue
		  response = given()
		  .header("Authorization", "Bearer " + bearerToken)
		  .header("Content-Type","application/json") 
		  .body("{\r\n"
					+ "\"title\":\""+title+"\",\r\n"
					+ "\"state_event\":\""+newstate+"\"\r\n"
					+ "}\r\n"
					+ "")
		  .when().put("projects/"+projectID+"/issues/"+iss_id)
		  .then().log().all()
		  .statusCode(statuscode);  // verify the status code
		 
	}
	
	@Then("Verify title state of issue")
	public void verify_title_state_of_issue(DataTable testdata) throws Throwable {
		
		String newstate;
		//Get the Data table from cucumber feature file into List
		List<List<String>> data = testdata.asLists(String.class);
		
		newstate = data.get(0).get(1); // the new state of issue to asserted
		JsonPath resp = response.extract().body().jsonPath();

		Assert.assertEquals(resp.getJsonObject("state").toString(), newstate); // assert new state with response of put api
		Assert.assertEquals(resp.getJsonObject("title").toString(), title); // assert new title with response of put api
	}
	
	@When("Delete the issue")
	public void delete_the_issue(DataTable testdata) throws Throwable {
			
		int iss_id = Integer.parseInt(issue_id);
		//Get the Data table from cucumber feature file into List
		List<List<String>> data = testdata.asLists(String.class);
		
		statuscode = Integer.parseInt(data.get(0).get(1)); // status code of delete issue api
		//delete the issue using delete issue api
		  response = given()
		  .header("Authorization", "Bearer " + bearerToken)
		  .header("Content-Type","application/json") 
		  .when().delete("projects/"+projectID+"/issues/"+iss_id)
		  .then().log().all()
		  .statusCode(statuscode);  
		 
	}
	
	@Given("Get the issue")
	public void Get_the_issue() throws Throwable {
		int iss_id = Integer.parseInt(issue_id);
		//after delete operation, get the same issue using issue id
		response = given()
		  .queryParam("iids[]", iss_id)
		  .header("Authorization", "Bearer " + bearerToken)
		  .when().get("issues/")
		  .then();
		 
	}
	
	@And("verify the issue deleted")
	public void verify_the_issue_deleted() throws Throwable {
		// verify the response of get api for an issue is empty
		 int new_current_number_of_issues = response.extract().jsonPath().getList("$").size();
		 Assert.assertEquals(new_current_number_of_issues, 0);
		 
	}
	
	@When("Subscribe to an issue")
	public void subscribe_to_an_issue() throws Throwable {
		
		int id = 904680586;
		int iss_id = Integer.parseInt(issue_id);  
		
		//Subscribe user to an issue
		  response = given()
		  .header("Authorization", "Bearer " + bearerToken)
		  .header("Content-Type","application/json") 
		  .body("{\r\n"
					+ "\"id\":\""+id+"\",\r\n"
					+ "\"issue_iid\":\""+iss_id+"\"\r\n"
					+ "}\r\n"
					+ "")
		  .when().post("projects/"+projectID+"/issues/"+iss_id+"/subscribe")
		  .then();	 
		 
	}
	
	@Then("Verify the status code")
	public void verify_the_status_code(DataTable testdata) throws Throwable {
		
		//Get the Data table from cucumber feature file into List
		List<List<String>> data = testdata.asLists(String.class);
				
		statuscode = Integer.parseInt(data.get(0).get(1)); // status code of subscribe issue api
		// verify the response status code
		 int actual_statuscode = response.extract().statusCode();
		 Assert.assertEquals(actual_statuscode, statuscode);
		 
	}
	
	@When("Move an issue")
	public void move_an_issue() throws Throwable {
		
		int id = 904680586;
		int iss_id = Integer.parseInt(issue_id);  
		
		//Subscribe user to an issue
		  response = given()
		  .header("Authorization", "Bearer " + bearerToken)
		  .header("Content-Type","application/json") 
		  .body("{\r\n"
					+ "\"id\":\""+id+"\",\r\n"
					+ "\"issue_iid\":\""+iss_id+"\",\r\n"
					+ "\"to_project_id\":\""+projectID+"\"\r\n"
					+ "}\r\n"
					+ "")
		  .when().post("projects/"+projectID+"/issues/"+iss_id+"/move")
		  .then();	 
		 
	}
	
	@Then("Verify the status code and response message")
	public void verify_the_status_code_and_response_message(DataTable testdata) throws Throwable {
		
		String messagedetails;
		//Get the Data table from cucumber feature file into List
		List<List<String>> data = testdata.asLists(String.class);
				
		statuscode = Integer.parseInt(data.get(0).get(1)); // status code of move issue api
		messagedetails = data.get(1).get(1);
		// verify the response status code
		 int actual_statuscode = response.extract().statusCode();
		 JsonPath resp = response.extract().body().jsonPath();
		 
		 Assert.assertEquals(resp.getJsonObject("message").toString(), messagedetails);
		 Assert.assertEquals(actual_statuscode, statuscode);
		 
	}
	
	@And("verify response time of move issue api")
	public void verify_response_time_of_move_issue_api(DataTable testdata) throws Throwable {
		
		Long responsetime;
		//Get the Data table from cucumber feature file into List
		List<List<String>> data = testdata.asLists(String.class);
				
		responsetime = Long.parseLong(data.get(0).get(1)); // responsetime of move issue api
		//assert actual and expected response time of move an issue api		 
		response.time(Matchers.lessThan(responsetime));
		 
	}
	
	@When("Create Issue in GitLab with title non_string")
	public void Create_Issue_in_GitLab_with_title_non_string(DataTable testdata) throws Throwable {
		//Get the Data table from cucumber feature file into List
		List<List<String>> data = testdata.asLists(String.class);
		
		//get the data table's data into individual variables like title of the issue
		title = data.get(0).get(1);
		description = data.get(1).get(1); //description of the issue

  
		//create new issue using post issues api
		  response = given()
		  .queryParam("title", "Issues with auth") 
		  .queryParam("labels", "bug")
		  .header("Authorization", "Bearer " + bearerToken)
		  .header("Content-Type","application/json") 
		  .body("{\r\n"
					+ "\"title\":"+title+",\r\n"
					+ "\"description\":\""+description+"\"\r\n"
					+ "}\r\n"
					+ "")
		  .when().post("projects/"+projectID+"/issues")
		  .then(); 
	}
	

}
